package com.jeespring.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jeespring.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/authentication", method = RequestMethod.GET)
	public void authentication(HttpServletRequest request , HttpServletResponse response) throws Exception {
		String signature = request.getParameter("signature").toString();
		String timestamp = request.getParameter("timestamp").toString();
		String nonce = request.getParameter("nonce").toString();
		String echostr = request.getParameter("echostr").toString();
		boolean authentication = userService.authentication(signature,timestamp,nonce,echostr);
		PrintWriter writer = response.getWriter();
		if (authentication) {
			writer.print(echostr);
		}else {
			writer.print("");
		}
	}

}
