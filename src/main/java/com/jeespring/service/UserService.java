package com.jeespring.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	
	private String token = "zhangchaoyang";
    Logger log = Logger.getLogger(UserService.class);  

	
	public boolean authentication(String signature,String timestamp,String nonce,String echostr) {
		List<String> list = new ArrayList<String>();
		list.add(token);
		list.add(timestamp);
		list.add(nonce);
		
		Collections.sort(list);
		
		log.info("排序后List：" + list);
		
		StringBuffer stringBuffer = new StringBuffer();
		
		for (String str : list) {
			stringBuffer.append(str);
		}
		
		log.info("排序后拼接的字符串：" + stringBuffer.toString());

		String sha1 = DigestUtils.sha1Hex(stringBuffer.toString());
		
		log.info("sha1加密后的字符串：" + sha1);
		
		if (signature.equals(sha1)) {
			return true;
		} else {
			return false;
		}
	}

}
